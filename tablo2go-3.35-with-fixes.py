#!/usr/bin/env python3

# TabloToGo version 3 - New Tablo APIs - J. Kenney 2016
# This uses the python requests library (pip install requests)

# Version 3 is a back-to-basics version due to new API
# Below are the editable options, stick with version 2
# If you require older options, Version 4 will be built
# to have a much nicer interface and capabilities, this
# is a proof of concept / test version.

                # 'series':disp_series,
                # 'season':disp_season,
                # 'episode':disp_episode,
                # 'title':disp_title,
                # 'year':disp_year,
                # 'duration':disp_duration,
                # 'height':disp_height,
                # 'description':disp_description,
                # 'date':disp_date,
                # 'date_only':disp_date_only,               # Example: 2016-11-23
                # 'network':disp_network,                   # Example: NBC
                # 'network_callsign':disp_network_callsign, # Example: WBAL-DT
                # 'channel':disp_channel                    # Example: 11.2

# Valid options, that can be placed in {} in NAME_SERIES and NAME_MOVIES are:
#   {series} {season} {episode} {title} {year} {date}
#   {network} {network_callsign} {channel}

# Note that any metadata apearing can be added manually to the code below, but
#   these are available by default.
NAME_SERIES = "/share/TiVo/{series}/{series} - S{season}E{episode} - {title}"
NAME_UNKNOWN = "/share/TiVo/{series}/{series} - {date_only} - {title}"
NAME_MOVIES = "/share/TiVo/Movies/{title} ({year})"
NAME_SPORTS  = "/share/TiVo/Sports/{series} - {date_only} - {title}"

# Do you want to use ffmpeg to download and encode the video?  This is the
# default method.  To change the type of file just change the extension below.
# If you would like a raw/perfect transport stream copy (.ts) set USE_FFMPEG to False
USE_FFMPEG = True

# Where is the ffmpeg executable located and what is the command that should be
# executed.  Use {build} for the filename without the extension, as {build} is
# the variable that will be built from the NAME_xxx above.  {m3u8} is the
# playlist for the actual videos on the tablo
FFMPEG = '/kmttg/ffmpeg'
FFMPEG_CMD = ' -i "{m3u8}" -c copy -bsf:a aac_adtstoasc "{build}.mp4"'

# Set minimum duration and quality measurements
# Duration in seconds, Quality based on height (480, 720, 1080, etc.)
MIN_DURATION = 600
MIN_QUALITY = 100

# Set metadata and history files
FILE_TABLO_METADATA = '/kmttg/tablo3.mtd'
FILE_TABLO_HISTORY = '/kmttg/tablo3.history'
FILE_TIVO_HISTORY = '/kmttg/auto.history'

# Delete file from tablo after download (Not yet enabled!)
DELETE_FROM_TABLO = False

# Perform metadata update only, do not download videos
LIST_ONLY = False

# Highlight failed recordings
SHOW_FAILED = False

# Default search specifications
# Types are movies, series, etc.
SEARCH = {'type':'',
          'id':'',
          'description':'',
          'series':'',
          'season':'',
          'episode':'',
          'title':'',
          'date':'',
          'year':'',
          'network':'',
          'network_callsign':'',
          'channel':''}

# These (all main variables, and search items)
# can now be configured via command line arguments
"""
Tablo2go <options> <search options>

Options: (Each of the main variables in lower case)
-name_series "value"        "/share/TiVo/{series}/{series} - S{season}E{episode} - {title}"
-name_movies "value"        "/share/TiVo/Movies/{title} ({year})"
-name_sports "value"        "/share/TiVo/Sports/{series} - {date_only} - {title}"
-name_unknown "value"       "/share/TiVo/{series}/{series} - {date_only} - {title}"
-ffmpeg "value"             "/kmttg/ffmpeg"
-ffmpeg_cmd "value"         ' -i "{m3u8}" -c copy -bsf:a aac_adtstoasc "{build}.mp4"'
-ip "value"                 192.168.1.10
-use_ffmpeg BOOL            Examples: true, false
-min_duration "value"       600
-min_quality "value"        100
-file_tablo_metadata "file" "/kmttg/tablo3.mtd"
-file_tablo_history "file"  "/kmttg/tablo3.history"
-file_tivo_history "file"   "/kmttg/auto.history"
-delete_from_tablo BOOL     Examples: true, false
-list_only BOOL             Examples: true, false
-quiet BOOL                 Examples: true, false
-debug BOOL                 Examples: true, false
-show_failed BOOL           Examples: true, false
-log_file "file"            Examples: "/usr/sample/tablo2go.log"

Search Flags:
--type "value"              Examples: series, movies, sports
--id "value"                Example: 257676
--series "value"            Example: "The Simpsons"
--season "value"            Examples: 02, 21
--episode "value"           Examples: 02, 21
--date "value"              Example: 2016-10-16T06:00Z
--year "value"              Example: 2016
--network "value"           Example: PBS
--network_callsign "value"  Example: WETAUK
--channel "value"           Example: 26.2

Examples:
 python3 tablo2go.py -list_only true --series "The Simpsons"
 python3 tablo2go.py -name_series "/share/TiVo/TV Shows/{series} - S{season}E{episode} - {title}" -name_unknown "/share/TiVo/TV Shows/{series} - {date_only}" --type "series"
 python3 tablo2go.py -name_movies "/share/TiVo/Movies/{title} ({year})" --type "movies"
 python3 tablo2go.py -name_sports "/share/TiVo/Sports/{series} - {date_only} - {title}" --type "sports"
"""

#############################################################
# Please do not edit below this line.
import os, sys, re, platform
import logging,logging.config,logging.handlers
if sys.version_info.major == 2:
    reload(sys)
    sys.setdefaultencoding('utf8')
try:
    import requests
except:
    log("Please install the requests library!")
    log("Use (for python3):")
    log("  pip3 install requests")
    log("or (for python2)")
    log("  pip install requests")
    sys.exit()
global DEBUG,FILELOGGER,LOGFILE_SIZE,NUM_LOGFILES
DEBUG = False
# FileLogger used for logging
FILELOGGER = None
# size in bytes
LOGFILE_SIZE=50000
# number of log files to keep
NUM_LOGFILES=5

# create the logger instance for logging to a rotating file
FILELOGGER = logging.getLogger("tablo2go")
# set to INFO by default
FILELOGGER.setLevel(logging.INFO)

#############################################################
# Add support for command line switches
argv = sys.argv[1:]
COMMAND_ARGS = {}
COMMAND_PROG = ''
FORCE_IP = ''

for i in range(len(argv)):
    if argv[i] != '*skip*':
        if argv[i].find('-') == 0:
            argv[i] = argv[i][1:]
            if argv[i].find('-') == 0:
                argv[i] = argv[i][1:]
                SEARCH[argv[i]] = argv[i+1]
            else:
                COMMAND_ARGS[argv[i]] = argv[i+1]
            argv[i+1] = '*skip*'
        else:
            COMMAND_PROG = COMMAND_PROG+' '+argv[i]
            COMMAND_PROG = COMMAND_PROG.strip()

if "ip" in COMMAND_ARGS:
    FORCE_IP = COMMAND_ARGS['ip']
if "name_series" in COMMAND_ARGS:
    NAME_SERIES = COMMAND_ARGS['name_series']
if "name_sports" in COMMAND_ARGS:
    NAME_SPORTS = COMMAND_ARGS['name_sports']
if "name_movies" in COMMAND_ARGS:
    NAME_MOVIES = COMMAND_ARGS['name_movies']
if "name_unknown" in COMMAND_ARGS:
    NAME_UNKNOWN = COMMAND_ARGS['name_unknown']
if "ffmpeg" in COMMAND_ARGS:
    FFMPEG = COMMAND_ARGS['ffmpeg']
if "ffmpeg_cmd" in COMMAND_ARGS:
    FFMPEG_CMD = COMMAND_ARGS['ffmpeg_cmd']
if "min_duration" in COMMAND_ARGS:
    MIN_DURATION = COMMAND_ARGS['min_duration']
if "min_quality" in COMMAND_ARGS:
    MIN_QUALITY = COMMAND_ARGS['min_quality']
if "file_tablo_metadata" in COMMAND_ARGS:
    FILE_TABLO_METADATA = COMMAND_ARGS['file_tablo_metadata']
if "file_tablo_history" in COMMAND_ARGS:
    FILE_TABLO_HISTORY = COMMAND_ARGS['file_tablo_history']
if "file_tivo_history" in COMMAND_ARGS:
    FILE_TIVO_HISTORY = COMMAND_ARGS['file_tivo_history']
if "use_ffmpeg" in COMMAND_ARGS:
    USE_FFMPEG = True
    if COMMAND_ARGS['use_ffmpeg'].upper() == 'FALSE':
        USE_FFMPEG = False
if "delete_from_tablo" in COMMAND_ARGS:
    DELETE_FROM_TABLE = False
    if COMMAND_ARGS['delete_from_tablo'].upper() == 'TRUE':
        DELETE_FROM_TABLO = True
if "list_only" in COMMAND_ARGS:
    LIST_ONLY = True
    if COMMAND_ARGS['list_only'].upper() == 'FALSE':
        LIST_ONLY = False
if "quiet" in COMMAND_ARGS:
    if COMMAND_ARGS['quiet'].upper() != 'TRUE':
        del(COMMAND_ARGS['quiet'])
if "debug" in COMMAND_ARGS:
    DEBUG = False
    if COMMAND_ARGS['debug'].upper() == 'TRUE':
        DEBUG = True
if "log_file" in COMMAND_ARGS:
    LOG_FILE = COMMAND_ARGS['log_file']
if "show_failed" in COMMAND_ARGS:
    SHOW_FAILED = False
    if COMMAND_ARGS['show_failed'].upper() == 'TRUE':
        SHOW_FAILED = True

#############################################################
# Process Search items
for key in SEARCH:
    SEARCH[key] = re.compile(SEARCH[key], re.IGNORECASE)

#############################################################
# Useful functions

# return a value in a python style dictionary
def rDict(_dict, _default, *sequence):
    if not sequence:
        return _default
    for item in sequence:
        if not item in _dict:
            return _default
        _dict = _dict[item]
    if _dict is None:
        return _default
    return _dict

# print out a python dictionary, in an rDict compatible format
def pDict(_dict, loc=0, dpath=''):
    if type(_dict) is not dict:
        log(dpath+'-> '),
        log(_dict)
    else:
        for item in _dict:
            pDict(_dict[item], loc+1, dpath+"'"+item+"', ")

# Clean a string of all bad characters
# Default ASCII ranges 48-57, 65-90, 97-122, - _ . are allowed,
#  otherwise BAD_CHARS can be defined directly
def clean(input, OVERRIDE={}):
    result = ''
    BAD_CHARS = {'(':'(', ')':')', ' ':' ', '"':' ', '&':'+', '/':' ', '\\':' ', '|':' ', "'":"", '?':'', u'\u2026':'', '@':'at ', u'\u2019':'',u'\xf8':'', u'\u2014':'-', ':':''}
    for key in OVERRIDE:
        BAD_CHARS[key] = OVERRIDE[key]
    lastchar = 'Z*Z'
    for char in input:
        o = ord(char)
        if (o >= 48 and o <= 57) or (o >= 65 and o <= 90) or (o >= 97 and o <= 122) or (o == 95) or (o == 46) or (o == 45) or char in BAD_CHARS:
            if char in BAD_CHARS:
                char = BAD_CHARS[char]
            if char in BAD_CHARS:
                char = BAD_CHARS[char]
            if lastchar != ' ' or char != ' ':
                result = result + char
            lastchar = char
    return result

def clean_for_file_name(_input):
    replace_chars = {}
    # if running on Windows apply the following rules
    if platform.system() == 'Windows':
        replace_chars = {':':'', '/':' ', '\\':' ', '?':'','*':'','"':"'",'<':'(','>':')','|':'-'}
    for char in replace_chars:
        _input = _input.replace(char, replace_chars[char])
        
    return _input
    
# Similiar to strip, will remove useless trailing characters
def squish(_input):
    _input = _input.strip()
    _input.replace('S00E00', '')
    while len(_input) > 0 and (_input[-1] == ' ' or _input[-1] == '-' or _input[-1] == '\n' or _input[-1] == '\r'):
        _input = _input[:-1]
    return _input

# Replace strings based on input from a dict
def fillin(_string, _dict):
    for key in _dict:
        if isinstance(_dict[key], str):
            _string = _string.replace('{'+key+'}', _dict[key])
        if (sys.version_info.major == 2 and isinstance(_dict[key], unicode)):
            _string = _string.replace('{'+key+'}', _dict[key])
        else:
            _string = _string.replace('{'+key+'}', str(_dict[key]))
    return _string
    
# Replace strings based on input from a dict
def fillin_for_file_name(_string, _dict):
    for key in _dict:
        if isinstance(_dict[key], str):
            _string = _string.replace('{'+key+'}', clean_for_file_name(_dict[key]))
        if (sys.version_info.major == 2 and isinstance(_dict[key], unicode)):
            _string = _string.replace('{'+key+'}', clean_for_file_name(_dict[key]))
        else:
            _string = _string.replace('{'+key+'}', clean_for_file_name(str(_dict[key])))
    return _string

# Retrieve information from a REST style API
# Return the results in a python style dictionary
def get_api(HOST, PORT, API_PATH, HTTPS=False, COMMENT=''):
    results = {}
    if HTTPS:
        CMD = 'https://' + HOST + ':' + str(PORT) + API_PATH
    else:
        CMD = 'http://' + HOST + ':' + str(PORT) + API_PATH
    resp = requests.get(CMD)
    if resp.status_code == 200:
        results = resp.json()
    if DEBUG:
        log('')
        if COMMENT != '':
            log(COMMENT)
        log('GET '+CMD)
        pDict(results)
    return results

# Retrieve information from a REST style API
# Return the results in a python style dictionary
def post_api(HOST, PORT, API_PATH, HTTPS=False, COMMENT=''):
    results = {}
    if HTTPS:
        CMD = 'https://' + HOST + ':' + str(PORT) + API_PATH
    else:
        CMD = 'http://' + HOST + ':' + str(PORT) + API_PATH
    resp = requests.post(CMD)
    if resp.status_code == 200:
        results = resp.json()
    if DEBUG:
        log('')
        if COMMENT != '':
            log(COMMENT)
        log('POST '+CMD)
        pDict(results)
    return results

# Download a video from a playlist
def get_video(pl, filename, comment=''):
    pls = ''
    resp = requests.get(pl)
    if resp.status_code == 200:
        for line in resp.iter_lines():
            line = line.decode('utf-8')
            if DEBUG:
                log(line)
            if line != '' and line.find("b'#") != 0 and line.find('#') != 0:
                if line.find("'") != -1:
                    line = line.split("'")[1]
                pls = pl.split('/stream/')[0]+line
    else:
        return 0
    if pls == '':
        return 0

    if DEBUG:
        log('')

    fileset = []
    resp = requests.get(pls)
    if resp.status_code == 200:
        for line in resp.iter_lines():
            line = line.decode('utf-8')
            if DEBUG:
                log(line)
            if line != '' and line.find("b'#") != 0 and line.find('#') != 0:
                if line.find("'") != -1:
                    line = line.split("'")[1]
                if line.find('/stream/') == 0:
                    fileset.append(pl.split('/stream/')[0]+line)
    else:
        return 0
    if fileset == []:
        return 0

    if DEBUG:
        log('')

    counter = 0
    with open(filename+'.ts', 'wb') as fileObject:
        for url in fileset:
            counter += 1
            update_progress(float(counter)/len(fileset), str(counter)+'/'+str(len(fileset))+" "+url.split('/stream')[-1])
            resp = requests.get(url, stream=True)
            for chunk in resp.iter_content(chunk_size=1024):
                if chunk:
                    fileObject.write(chunk)

    return os.path.isfile(filename+'.ts')

# Retrieve a stored
def load_metadata(filename='tablo3.mtd'):
    try:
        results = open(filename,'r').readlines()
        results = eval(results[0])
        return results
    except:
        return {}

# Save metadata
def save_metadata(METADATA, filename='tablo3.mtd'):
    try:
        results = open(filename, 'w')
        results.write(str(METADATA)+'\n')
        results.close()
    except:
        pass

# Load History
def load_history(filename, results={}):
    try:
        hdata = open(filename, 'r').readlines()
        for line in hdata:
            line = line.strip()
            if line.find(' ') != -1:
                tmp = line.split()
                results[tmp[0]] = line
    except:
        pass
    return results

# Update History
def update_history(filename, tmsid, desc):
    hdata = open(filename, 'a')
    hdata.write(tmsid+' '+desc+'\n')
    hdata.close()

# Progress bar (http://stackoverflow.com/questions/3160699/python-progress-bar)
def update_progress(progress,status=""):
    status = clean(status)
    barLength = 10
    if isinstance(progress, int):
        progress = float(progress)
    if progress < 0:
        progress = 0
        status = status + " Halt...\r\n"
    if progress >= 1:
        progress = 1
        status = status + " Done...\r\n"
    block = int(round(barLength*progress))
    text = "\rPercent: [{0}] {1}% {2}".format( "#"*block + "-"*(barLength-block), round(progress*100,2), status)
    sys.stdout.write(text)
    sys.stdout.flush()

# if debugging enabled then log the message
def debug(output):
    if DEBUG:
        print(output)
        if 'log_file' in COMMAND_ARGS:
            FILELOGGER.debug(output)

# log the message
def log(output):
    print(output)
    if 'log_file' in COMMAND_ARGS:
        FILELOGGER.info(output)
    
# setup the file logger
if 'log_file' in COMMAND_ARGS:
    # Add the rotating log handler
    handler = logging.handlers.RotatingFileHandler(LOG_FILE, maxBytes=LOGFILE_SIZE, backupCount=NUM_LOGFILES)
    # set the formatting template on the handler
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # tell the handler to use this format
    handler.setFormatter(formatter)
    # add the rotatng file handler to the logger
    FILELOGGER.addHandler(handler)
    # change the file logger log level to capture DEBUG level messages
    FILELOGGER.setLevel(logging.DEBUG)
    
# Load metadata if stored
HISTORY = load_history(FILE_TABLO_HISTORY)
HISTORY = load_history(FILE_TIVO_HISTORY, HISTORY)
METADATA = load_metadata(FILE_TABLO_METADATA)
METADATA_UPDATED = False
METADATA_VALID = {}
QUEUE = {}

# Acquire a list of available tablo's
found_tablos = get_api('api.tablotv.com','443','/assocserver/getipinfo/', HTTPS=True, COMMENT='Retrieve listing of available Tablos')

if FORCE_IP != '':
    found_tablos = {"cpes": [{"http": 31880, "public_ip": FORCE_IP, "ssl": 0, "host": "tablo-quad", "private_ip": FORCE_IP, "slip": 31887, "serverid": "SID_XXX", "inserted": "2014-07-25 15:04:17.748376+00:00", "server_version": "2.2.11rc1628712", "name": "Tablo", "modified": "2016-10-29 19:22:45.410564+00:00", "roku": 0, "board": "quad", "last_seen": "2016-10-29 19:22:45.406336+00:00"}], "success": True}

found_tablos = rDict(found_tablos, [], 'cpes')

# Download Metadata from found tablos
for tablo in found_tablos:
    TABLO_IP = tablo['private_ip']
    QUEUE[TABLO_IP] = {}
    METADATA_VALID[TABLO_IP] = {'series':{}, 'airing':{}, 'season':{}, 'sport':{}}
    if TABLO_IP not in METADATA:
        METADATA[TABLO_IP] = {}
    if 'series' not in METADATA[TABLO_IP]:
        METADATA[TABLO_IP]['series'] = {}
    if 'airing' not in METADATA[TABLO_IP]:
        METADATA[TABLO_IP]['airing'] = {}
    if 'season' not in METADATA[TABLO_IP]:
        METADATA[TABLO_IP]['season'] = {}
    if 'sport' not in METADATA[TABLO_IP]:
        METADATA[TABLO_IP]['sport'] = {}
    METADATA[TABLO_IP]['api.tablotv.com'] = tablo
    METADATA[TABLO_IP]['info'] = get_api(TABLO_IP, '8885', '/server/info', COMMENT='Retrieve tablo device information')
    if 'quiet' not in COMMAND_ARGS:
        log(TABLO_IP + ' - Found Tablo - ' + rDict(tablo, 'unk-name', 'name') + ' ('+rDict(tablo, 'unk-type', 'board')+')')

    series_list = get_api(TABLO_IP, '8885', '/recordings/shows', COMMENT='Retrieve a list of recorded shows')
    series_count = 0
    for series in series_list:
        series_count += 1
        series_num = series.split('/')[-1]
        METADATA_VALID[TABLO_IP]['series'][series_num] = 1
        if series_num not in METADATA[TABLO_IP]['series']:
            METADATA_UPDATED = True
            METADATA[TABLO_IP]['series'][series_num] = get_api(TABLO_IP, '8885', series, COMMENT='Retrieve series information')
            if DEBUG:
                log(TABLO_IP + ' - Loaded Series Metadata for ' + rDict(METADATA[TABLO_IP]['series'][series_num], 'unk-title', 'series', 'title'))
            else:
                update_progress(float(series_count)/len(series_list), "Loading Series Metadata  ")

    airings_list = get_api(TABLO_IP, '8885', '/recordings/airings', COMMENT='Retrieve a list of airings')
    airings_count = 0
    for airing in airings_list:
        airings_count += 1
        airing_type = airing.split('/')[2]
        airing_num = airing.split('/')[-1]
        airing_show = False
        METADATA_VALID[TABLO_IP]['airing'][airing_num] = 1
        if airing_num not in METADATA[TABLO_IP]['airing'] or (rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'video_details', 'state') != 'finished' and rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'video_details', 'state') != 'failed'):
            METADATA[TABLO_IP]['airing'][airing_num] = get_api(TABLO_IP, '8885', airing, COMMENT='Retrieve specific airing information')
            airing_show = True
            METADATA_UPDATED = True
        # Get season info from the episode
        season = rDict(METADATA[TABLO_IP]['airing'][airing_num], '/unk-season', 'season_path')
        season_num = season.split('/')[-1]
        METADATA_VALID[TABLO_IP]['season'][season_num] = 1
        if season_num not in METADATA[TABLO_IP]['season']:
            METADATA[TABLO_IP]['season'][season_num] = get_api(TABLO_IP, '8885', season, COMMENT='Retrieve season information')
            METADATA_UPDATED = True
        METADATA[TABLO_IP]['airing'][airing_num]['season_info'] = METADATA[TABLO_IP]['season'][season_num]
        # Get sports info from the event
        sport = rDict(METADATA[TABLO_IP]['airing'][airing_num], '/unk-sports', 'sport_path')
        sport_num = sport.split('/')[-1]
        METADATA_VALID[TABLO_IP]['sport'][sport_num] = 1
        if sport_num not in METADATA[TABLO_IP]['sport']:
            METADATA[TABLO_IP]['sport'][sport_num] = get_api(TABLO_IP, '8885', sport, COMMENT='Retrieve sport information')
            METADATA_UPDATED = True
        METADATA[TABLO_IP]['airing'][airing_num]['sport_info'] = METADATA[TABLO_IP]['sport'][sport_num]
        
        airing_series = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'series_path')
        airing_series = airing_series.split('/')[-1]
        METADATA[TABLO_IP]['airing'][airing_num]['series_info'] = rDict(METADATA[TABLO_IP]['series'], {}, airing_series)
        if airing_type == 'movies' and 'movie_info' not in METADATA[TABLO_IP]['airing'][airing_num]:
            METADATA[TABLO_IP]['airing'][airing_num]['movie_info'] = get_api(TABLO_IP, '8885', rDict(METADATA[TABLO_IP]['airing'][airing_num], 'unk-movie', 'movie_path'), COMMENT='Retrieve movie information')
        disp_series = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'series_info', 'series', 'title')
        disp_series = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_series, 'sport_info', 'sport', 'title')
        disp_season = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'season_info', 'season', 'number')
        disp_season = str(disp_season).zfill(2)
        disp_episode = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'episode', 'number')
        disp_episode = str(disp_episode).zfill(2)
        disp_title = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'episode', 'title')
        disp_title = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_title, 'movie_info', 'movie', 'title')
        disp_title = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_title, 'event', 'title')
        disp_series_date = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'series_info', 'series', 'orig_air_date')
        disp_series_year = disp_series_date[:4]
        disp_year = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'movie_info', 'movie', 'release_year')
        disp_duration = rDict(METADATA[TABLO_IP]['airing'][airing_num], 0, 'video_details', 'duration')
        disp_height = rDict(METADATA[TABLO_IP]['airing'][airing_num], 0, 'video_details', 'height')
        disp_state = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'video_details', 'state')
        disp_description = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'episode', 'description')
        disp_description = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_description, 'movie_info', 'movie', 'plot')
        disp_date = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'airing_details', 'datetime')
        disp_date_only = disp_date[:10]
        disp_id = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'episode', 'tms_id')
        disp_id = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_id, 'movie_airing', 'tms_id')
        disp_id = rDict(METADATA[TABLO_IP]['airing'][airing_num], disp_id, 'event', 'tms_id')
        disp_channel_path = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'channel_path')
        disp_network = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'channel', 'network')
        disp_network_callsign = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'channel', 'call_sign')
        disp_channel_major = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'channel', 'major')
        disp_channel_minor = rDict(METADATA[TABLO_IP]['airing'][airing_num], '', 'channel', 'minor')
        disp_channel = disp_channel_major+'.'+disp_channel_minor
        if disp_state == 'finished':        # options are finished, recording, and failed
            QUEUE[TABLO_IP][airing_num] = {
                'series':disp_series,
                'series_date':disp_series_date,
                'series_year':disp_series_year,
                'season':disp_season,
                'episode':disp_episode,
                'title':disp_title,
                'year':disp_year,
                'duration':disp_duration,
                'height':disp_height,
                'description':disp_description,
                'date':disp_date,
                'date_only':disp_date_only,
                'type':airing_type,
                'network':disp_network,
                'network_callsign':disp_network_callsign,
                'channel':disp_channel}
            if airing_type == 'movies':
                disp_build = squish(fillin_for_file_name(NAME_MOVIES, QUEUE[TABLO_IP][airing_num]))
                if disp_id == '':
                    disp_id = clean(disp_build, {' ':'.'})
            elif airing_type == 'series' and disp_season == '00' and disp_episode == '00':
                disp_build = squish(fillin_for_file_name(NAME_UNKNOWN, QUEUE[TABLO_IP][airing_num]))
                if disp_id == '' or disp_id.find('SH') == 0:
                    disp_id = clean(disp_build, {' ':'.'})
            elif airing_type == 'series':
                disp_build = squish(fillin_for_file_name(NAME_SERIES, QUEUE[TABLO_IP][airing_num]))
                if disp_id == '' or disp_id.find('SH') == 0:
                    disp_id = clean(disp_build, {' ':'.'})
            elif airing_type == 'sports':
                disp_build = squish(fillin_for_file_name(NAME_SPORTS, QUEUE[TABLO_IP][airing_num]))
                if disp_id == '' or disp_id.find('SH') == 0:
                    disp_id = clean(disp_build, {' ':'.'})
            
            if disp_state == 'finished':
                QUEUE[TABLO_IP][airing_num]['id'] = disp_id
                QUEUE[TABLO_IP][airing_num]['build'] = disp_build
            if airing_show:
                if DEBUG:
                    log(TABLO_IP+' - Loaded Metadata - '+disp_build)
                else:
                    update_progress(float(airings_count)/len(airings_list), "Loading Episode Metadata  ")
        if disp_state == 'failed' and SHOW_FAILED:
            log('')
            log(TABLO_IP+' - Failed Recording - '+disp_build)
            pDict(METADATA[TABLO_IP]['airing'][airing_num])

# Determine if there is any unnecessary metadata that should be removed
METADATA_DELETE = []
for TABLO_IP in METADATA:
    for mode in ('series', 'airing', 'season'):
        for item in METADATA[TABLO_IP][mode]:
            if rDict(METADATA_VALID, 'delete', TABLO_IP, mode, item) == 'delete':
                METADATA_DELETE.append([TABLO_IP, mode, item])
                METADATA_UPDATED = True

# Delete unnecessary metadata
if found_tablos != []:
    for line in METADATA_DELETE:
        del(METADATA[line[0]][line[1]][line[2]])

# save metadata to file if there are any changes
if METADATA_UPDATED:
    save_metadata(METADATA, FILE_TABLO_METADATA)

# Go through queued items
for TABLO_IP in QUEUE:
    items_downloaded = 0
    # Sort by date, most recent first
    SORTER = []
    for airing_num in QUEUE[TABLO_IP]:
        SORTER.append([QUEUE[TABLO_IP][airing_num]['date']+airing_num, QUEUE[TABLO_IP][airing_num]['date'], airing_num])
    SORTER.sort()
    for airing_num_data in SORTER:
        dtg = airing_num_data[1]
        dtg = dtg.replace('T',' ')
        airing_num = airing_num_data[2]
        height = QUEUE[TABLO_IP][airing_num]['height']
        duration = QUEUE[TABLO_IP][airing_num]['duration']
        history_id = QUEUE[TABLO_IP][airing_num]['id']
        match_failed = False;
        for search_criteria in SEARCH:
            if SEARCH[search_criteria] != '':
                if search_criteria in QUEUE[TABLO_IP][airing_num]:
                    if (sys.version_info.major == 2 and isinstance(QUEUE[TABLO_IP][airing_num][search_criteria], unicode)) or isinstance(QUEUE[TABLO_IP][airing_num][search_criteria], str):
                        if not SEARCH[search_criteria].search(QUEUE[TABLO_IP][airing_num][search_criteria]):
                            match_failed = True;
                    elif not SEARCH[search_criteria].search(str(QUEUE[TABLO_IP][airing_num][search_criteria])):
                        match_failed = True;
        if duration >= MIN_DURATION and height >= MIN_QUALITY and not match_failed and history_id not in HISTORY:
            playlist = {}
            if QUEUE[TABLO_IP][airing_num]['type'] == 'series':
                playlist = post_api(TABLO_IP, '8885', '/recordings/series/episodes/'+str(airing_num)+'/watch', COMMENT='Retrieve series playlist')
            elif QUEUE[TABLO_IP][airing_num]['type'] == 'movies':
                playlist = post_api(TABLO_IP, '8885', '/recordings/movies/airings/'+str(airing_num)+'/watch', COMMENT='Retrieve movie playlist')
            elif QUEUE[TABLO_IP][airing_num]['type'] == 'sports':
                playlist = post_api(TABLO_IP, '8885', '/recordings/sports/events/'+str(airing_num)+'/watch', COMMENT='Retrieve sports playlist')
            QUEUE[TABLO_IP][airing_num]['m3u8'] = rDict(playlist, '', 'playlist_url')
            run_command = FFMPEG + ' ' + FFMPEG_CMD
            run_command = fillin(run_command, QUEUE[TABLO_IP][airing_num])
            log('')
            log(TABLO_IP+' - '+ airing_num + ' - '+ dtg + ' - ' +history_id+' - Downloading - ' + QUEUE[TABLO_IP][airing_num]['build'])
            if os.path.isfile(QUEUE[TABLO_IP][airing_num]['build']+'.ts'):
                log(TABLO_IP+' - Download Aborted File Already Exists, Marking as Complete')
                update_history(FILE_TABLO_HISTORY,history_id, QUEUE[TABLO_IP][airing_num]['build']+" * Saved already by Tablo (ts)")
            elif os.path.isfile(QUEUE[TABLO_IP][airing_num]['build']+'.mp4'):
                log(TABLO_IP+' - Download Aborted File Already Exists, Marking as Complete')
                update_history(FILE_TABLO_HISTORY,history_id, QUEUE[TABLO_IP][airing_num]['build']+" * Saved already by Tablo (mp4)")
            elif os.path.isfile(QUEUE[TABLO_IP][airing_num]['build']+'.mkv'):
                log(TABLO_IP+' - Download Aborted File Provided By TiVo, Marking as Complete')
                update_history(FILE_TABLO_HISTORY,history_id, QUEUE[TABLO_IP][airing_num]['build']+" * Saved already by TiVo (mkv)")
            elif not LIST_ONLY:
                try:
                    os.makedirs(os.path.dirname(QUEUE[TABLO_IP][airing_num]['build'])) # , exist_ok=True)
                except:
                    pass
                if USE_FFMPEG:
                    os.system(run_command)
                    if True or os.path.isfile(QUEUE[TABLO_IP][airing_num]['build']+'.mp4'):
                        log(TABLO_IP+' - Updating History file - '+QUEUE[TABLO_IP][airing_num]['build']+'.mp4')
                        update_history(FILE_TABLO_HISTORY,history_id, QUEUE[TABLO_IP][airing_num]['build'])
                        items_downloaded +=1
                elif (get_video(QUEUE[TABLO_IP][airing_num]['m3u8'], QUEUE[TABLO_IP][airing_num]['build'])):
                        log(TABLO_IP+' - Updating History file - '+QUEUE[TABLO_IP][airing_num]['build']+'.ts')
                        update_history(FILE_TABLO_HISTORY,history_id, QUEUE[TABLO_IP][airing_num]['build'])
                        items_downloaded +=1
                else:
                    log(TABLO_IP+' - Download Failed - '+QUEUE[TABLO_IP][airing_num]['build']+'.ts')
        elif not DEBUG:
            quietmode = True
        elif duration < MIN_DURATION:
            log(TABLO_IP+' - '+ airing_num + ' - ' + dtg + ' - '  + history_id+' - Video Length ('+str(duration)+') to Short! - ' + QUEUE[TABLO_IP][airing_num]['build'])
        elif height < MIN_QUALITY:
            log(TABLO_IP+' - '+ airing_num + ' - ' + dtg + ' - '  + history_id+' - Video Quality ('+str(height)+') to Low! - ' + QUEUE[TABLO_IP][airing_num]['build'])
        elif match_failed:
            log(TABLO_IP+' - '+ airing_num + ' - ' + dtg + ' - '  + history_id+' - Failed Match Criteria - ' + QUEUE[TABLO_IP][airing_num]['build'])
        elif history_id in HISTORY:
            log(TABLO_IP+' - '+ airing_num + ' - ' + dtg + ' - '  + history_id+' - Already Downloaded ' + QUEUE[TABLO_IP][airing_num]['build'])
    log(TABLO_IP+' - ' + str(items_downloaded) + ' videos downloaded')